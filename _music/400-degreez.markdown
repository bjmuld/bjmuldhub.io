---
title: '400 Degreez:  Revisiting Juvie'
date: 2017-07-05 09:52:00 Z
description: You’ll find this post in your `_posts` directory. Go ahead and edit it
  and re-build the site to see your changes.
images:
- "/uploads/400deg.jpg"
layout: musicLayout
---

 This is a placeholder Music/ post

> I was listening to Ministry and Garth Brooks and Charlie Pride and Wynton Marsalis, and then I would listen to Juvenile or Lil Wayne. It's just that I'm a big fan of music. I'm a student of music. And I just want to learn and keep enhancing my education about the music.
<cite>- Trombone Shorty</cite>

![juvie.jpg](/uploads/juvie.jpg){:width="30%"}